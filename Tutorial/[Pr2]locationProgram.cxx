#ifndef LOCATION_HXX
#define LOCATION_HXX

#include <iostream>
#include <string>
#include <list>
#include <algorithm>
#include <vector>
 
// Realizado en clase con el tutor Sebastian Marichal
class Location{

	std::string _name;

	public:
		Location()
			:_name("unknown")
		{}		
		

		const std::string & name() const
		{
			return _name;
		}

		void name(const std::string &name) 
		{
			_name = name;
		}	

		std::string description()
		{
			return "Location: " + _name + "\n";
		}

};
 
#endif

typedef std::list<Location*> Items;

int main(void) {

    Items locations;
    int num,i;
    std::string nombreLocalizacion,auxiliar;
    
    std::cin>>num>> std::ws;    
    for(i=0;i<num;i++){ 
        std::getline( std::cin, nombreLocalizacion);
        auxiliar = nombreLocalizacion;
        Location *nombreLocalizacion = new Location(); 
        nombreLocalizacion->name(auxiliar);
        locations.push_back(nombreLocalizacion);          
    }
    for (Items::iterator it=locations.begin(); it!=locations.end(); it++){ 
	std::cout <<"Location: "<< (*it)->name()<<"\n";         
    }
}
