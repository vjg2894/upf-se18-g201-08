
#ifndef animals_hxx
#define animals_hxx

#include <iostream>
#include <string>
#include <list>
#include <algorithm>
#include <vector>
    
class Animal
{
public: 
	virtual void writeSpecies() const
	{
		std::cout << "...undefined..." << std::endl;
	}
	void name(std::string theName)
	{
		_name = theName;
	}
	std::string& name()
	{
		return _name;
	}
        void frien(std::string theFrien)
	{
		_frien.push_back(theFrien);
	}
	std::string frien()
	{
		if(_frien.size()!=0){
                    
                 std::string frien=_frien[0]; 
                 for(int i=1; i<_frien.size();i++){
                        frien.append(", "+_frien[i]);
                    }
                return frien;
                }
                return "";
	}
private:
	std::string _name;
        std::vector <std::string> _frien = std::vector <std::string> () ; 
        
};
class Elephant : public Animal
{
public:
	void writeSpecies() const
	{
		std::cout << "elephant" << std::endl;
	}
};
class Frog : public Animal
{
public:
	void writeSpecies() const
	{
		std::cout << "frog" << std::endl;
	}
};
class Cat : public Animal
{
public:
	void writeSpecies() const
	{
		std::cout << "cat" << std::endl;
	}
};

#endif

typedef std::list<Animal*> Animals;

int main(void)
{
        int num,num2;
	Animals animals;
        std::string nombreAnimal;
        std::string amigoAnimal;
        std::string parte1,parte2,parte3,parte4,parte5,parte6;
        std::string parteAuxiliar;
        int posicion;
        int i=0;
        
        std::cin>> num >> std::ws;
        while(i<num){
            std::getline( std::cin, nombreAnimal );
            posicion = nombreAnimal.find(" ");
            parte1 = nombreAnimal.substr(posicion);
            parte3 = nombreAnimal.substr(0,posicion);
            parte2 = parte1.substr(1);
            parteAuxiliar = parte2;
            
            if(parte3=="Animal"){
                Animal *parte2 = new Animal(); 
                parte2->name(parteAuxiliar); 
                animals.push_back(parte2);
            }else if(parte3=="Elephant"){
                Elephant *parte2 = new Elephant();
                parte2->name(parteAuxiliar);
                animals.push_back(parte2);
            }else if(parte3=="Frog"){
                Frog *parte2 = new Frog();
                parte2->name(parteAuxiliar);
                animals.push_back(parte2);
            }else if(parte3=="Cat"){
                Cat *parte2 = new Cat();
                parte2->name(parteAuxiliar);
                animals.push_back(parte2);
            }
            i++;
        }
        i=0;
        std::cin>> num2 >> std::ws; 
        while(i<num2){ 
            std::getline( std::cin, amigoAnimal ); 
            posicion = amigoAnimal.find("-"); 
            parte4 = amigoAnimal.substr(posicion);
            parte5 = amigoAnimal.substr(0,posicion);
            parte6 = parte4.substr(1);
         for (Animals::iterator it=animals.begin(); it!=animals.end(); it++)
            {
              if(parte5 == (*it)->name()){ 
                 (*it)->frien(parte6);
                for (Animals::iterator at=animals.begin(); at!=animals.end(); at++)
                {
                    if(parte6 == (*at)->name()){   
                        (*at)->frien(parte5);
                    }
                }
                  break;
              }
            }
            i++;
        }     
         for (Animals::iterator it=animals.begin(); it!=animals.end(); it++)
	{
		std::cout << (*it)->name() << " is of the species ";
		(*it)->writeSpecies();
                if((*it)->frien() != ""){
                    std::cout << (*it)->name() << " is friends with "<<(*it)->frien()<<std::endl;
                }
	}
	return 0;
}

