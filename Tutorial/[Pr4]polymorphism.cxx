/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   polymorphism.cxx
 * Author: Victor
 *
 *
 */

#include <cstdlib>
#include <sstream>
#include <vector>
#include <iostream>
#include <string>
#include <list>
#include <algorithm>

class LocationNotFound;
class CharacterNotFound;
class Character;
class AbstractItem;
class DecoratorItem;
class Item;
class World;
class Location;


typedef std::vector<Location *> Locations; 
typedef std::vector<Character *> Characters; 
typedef std::vector<AbstractItem *> Items;


class CharacterNotFound :  public std::exception
{
	public: const char * what() const throw () { return "The character does not exist"; }
};



class ItemNotFound :  public std::exception
{
	public: const char * what() const throw () { return "The item does not exist"; }
};



class LocationNotFound :  public std::exception
{
	public: const char * what() const throw () { return "The location does not exist"; }
};


class AbstractItem {

	public:
		virtual const std::string & getName() const=0;
		virtual void setName(const std::string &name) =0;		
		virtual const unsigned int & level() const=0;
		virtual void level(const unsigned int &level) =0;
		virtual const bool & hasItem() const=0;
		virtual void hasItem(const bool &hasItem) =0;		
		virtual std::string description () =0;
   		virtual std::string useItem(Character * c, Location * location)=0;
		virtual std::string receiveMagic ( const unsigned int npts )=0;
		virtual bool status ()=0;
		virtual void status ( bool nEstado ) =0;
		virtual ~AbstractItem() {}; 

};


class Item : public AbstractItem{
	
	protected:
		std::string _name;
		unsigned int _level;
		bool _hasItem;
		bool estado;

	public:
		Item () {
			_name = "unknown";
			_level = 75;
			estado = false;
			_hasItem = false;
		}

		Item ( const std::string & newName, const unsigned int newLevel ) {
			_name = newName;
			_level = newLevel;
			estado = false;
		}

		
		const std::string & getName() const { return _name; }
		void setName(const std::string &name) { _name = name; }
		const unsigned int & level() const { return _level; } 
		void level(const unsigned int &level) { _level = level; }
		const bool & hasItem() const { return _hasItem; }
		void hasItem(const bool &hasItem) { _hasItem = hasItem; }
		bool status () { return estado;  }
		void status ( bool nEstado ) { estado = nEstado; }
		std::string useItem( Character * c,  Location * location);

		std::string description () 
   		{
   		 	std::stringstream stream;    
    		stream << getName() << " [" << level() << "]";
     		return stream.str();
   		}
   		
		std::string receiveMagic ( const unsigned int npts )
		{
			std::stringstream points;
			points << npts;
			return _name + " receives " + points.str() + " magic points\n";
		}
 
};
  



class ItemDecorator : public AbstractItem {
	
	protected:
		AbstractItem * itemAbstracto;

	public:

		ItemDecorator ( AbstractItem * item ){ itemAbstracto = item; }
		const std::string & getName() const { return itemAbstracto->getName(); }
		void setName(const std::string &name) { return itemAbstracto->setName(name);}
		const unsigned int & level() const{  return itemAbstracto->level();}
		void level(const unsigned int &level) {      return itemAbstracto->level(level);}		 
		const bool & hasItem() const{ return itemAbstracto->hasItem();}
		void hasItem(const bool &hasItem) { return itemAbstracto->hasItem(hasItem);}
		std::string description () { return itemAbstracto->description();}
		std::string useItem(Character * c,  Location * location){ return itemAbstracto->useItem(c, location);}
		std::string receiveMagic ( const unsigned int npts ){ return itemAbstracto->receiveMagic(npts);}
		bool status () {  return itemAbstracto->status(); }
		void status ( bool nEstado ) {  return itemAbstracto->status(nEstado); }
		~ItemDecorator(){ if ( itemAbstracto != NULL ) {delete itemAbstracto; }}
	
};

class Trap : public ItemDecorator {
	
public:
	Trap( AbstractItem * absItem ) : ItemDecorator(absItem){}
	std::string useItem(Character * c, Location * location );
}; 


class Bomb : public ItemDecorator {

public:
	Bomb( AbstractItem * absItem ) : ItemDecorator(absItem){}
	std::string useItem( Character * c, Location * l  );
	std::string receiveMagic( const unsigned int p );

};


class Potion : public ItemDecorator {
	
public:
	Potion( AbstractItem * absItem ) : ItemDecorator(absItem){}
	std::string useItem( Character * c ,  Location * l);
};


class Character{

	protected:
		std::string _name;
		unsigned int _level;
		unsigned int _life;
		unsigned int _damage;
		unsigned int _cure;
		Location * loc_;

	public:
		
		Character()
			:_name("unknown"), _level(0u), _life(10u), loc_(0)     
		{
 			// TO DO
		}	

	
		const std::string & name() const{ return _name; }
		void name(const std::string &name) { _name = name; }
		const unsigned int & level() const{ return _level; }
		void level(const unsigned int &level) { _level = level; }
		const unsigned int & life() { return _life; }
		void life(const unsigned int &life) { _life = life; }
		void locateAt( Location & newLoc );

		std::string cure(const unsigned int &cure) 
		{
			if(_life==10){ _life = _life; }
			else{ _life = _life + cure; }
			std::stringstream ss;
			ss << cure;

			return " gains " + ss.str() + " life\n";
		}
 
		virtual std::string receiveMagic(unsigned int points)
		{
			std::stringstream ss;
			ss << this->name() << " receives " << points << " magic points\n";
			return ss.str();
		}

		std::string damage(const unsigned int &damage) 
		{
			if(damage > _life ){ _life = 0; }
			else { _life = _life-damage; }

			std::stringstream ss;
			ss << damage;
			return this->name() + " takes " + ss.str() + " damage\n";
		}

		virtual ~Character(){};

};


class Cure : public Character {

public:

	Cure ( const std::string newName, const unsigned int newLevel ) {
		_name = newName;
		_level = newLevel;
	} 

	std::string receiveMagic ( unsigned int p )
	{
		cure( p );

		std::stringstream points;
		points << p;

		return 	Character::receiveMagic( p ) + _name + " gains " + points.str() + " life\n";
	}

};


class CharacterDamage : public Character
{
	
	public:
		CharacterDamage ( std::string newName, unsigned int newLevel) 
		{  
			_name = newName;
			_level = newLevel;
		}


		std::string receiveMagic(unsigned int damg)
		{
			//printf("llego");
			damage(damg);  

			std::stringstream damageChar;
			damageChar << damg;
			return Character::receiveMagic(damg) + _name  + " takes " + damageChar.str() + " damage\n";
		}

};


class Location{

	protected: 
		std::string _name;      
		Location* _north;
		Location* _south;  
		Location* _west;
		Location* _east;
		Items _items;
		Characters _characters;

	public:
		Location()
			:_name("unknown"),_north(NULL), _south(NULL), _west(NULL), _east(NULL)
		{}		
		
		Location ( const std::string &newName ) {
			_name = newName;
			_north = NULL;
			_south = NULL;
			_east = NULL;
			_west = NULL;
		}

		const std::string & name() const { return _name; }
		void name(const std::string &name) { _name = name; }			
		void connectNorth(Location &norte) { _north = &norte; }
		Location* north() { return _north; }
		void connectSouth(Location &sur) { _south = &sur; }
		Location* south() { return _south; }
		void connectWest(Location &west) { _west = &west; }
		Location* west() { return _west; }
		void connectEast(Location &east) {  _east = &east; }
		Location* east() { return _east; }

		std::string description()
		{
			std::string description = "Location: " + _name + "\n";
			description += connections();
			description += items();
			description += characters();
			return description;
		}

		std::string connections () const {
			std::string connections = "";
			if (_north != NULL ){connections += "\tNorth: " + _north->name() + "\n";}
			if (_south != NULL ){connections += "\tSouth: " + _south->name() + "\n";}
			if (_east != NULL ){connections += "\tEast: " + _east->name() + "\n";}
			if (_west != NULL ){connections += "\tWest: " + _west->name() + "\n";}
			return connections; 
		}
		
		void addItem(const std::string & name,const unsigned int level)
		{
			AbstractItem * item = new Item(name, level);
			_items.push_back(item);
		}
		
		std::string items() const
		{
			std::string out = "";
			for(Items::const_iterator it = _items.begin(); it!= _items.end(); it++)
			{
				out += "\tItem: " + (*it)->description() + "\n";
			}
			return out;
		}
		
		AbstractItem & findItem(const std::string &findName)
		{
			for(Items::const_iterator it = _items.begin(); it!= _items.end(); it++){
				if((*it)->getName() == findName){
					return (**it);			
				}
			}
			throw ItemNotFound();
		}
		
		void placeCharacter(Character & placedCharacter)
		{
			Character * characterPl = &placedCharacter;
			_characters.push_back( characterPl );
		}

		
		std::string characters()
		{
			std::string characters;

			for(Characters::iterator it = _characters.begin(); it != _characters.end(); ++it){
				characters += "- " + (*it)->name() + " is here.\n";
			}
			return characters;
		}
		
		Character & findCharacter(const std::string &findName){
			for(Characters::iterator it = _characters.begin(); it!= _characters.end(); it++){
				if((*it)->name() == findName){
					return (**it);			
				}
			}
			throw CharacterNotFound();
			
		}
		
		void unplaceCharacter(Character & unplacedCharacter){
			for(Characters::iterator it = _characters.begin(); it!= _characters.end(); it++){
				if((*it)->name() == unplacedCharacter.name()){
					it = _characters.erase(it);
					return;			
				}
			}
			throw CharacterNotFound();
			
		}

		std::string distributeMagic ( int npts )
		{
			std::string d = ""    ;

			for ( Characters::iterator it = _characters.begin(); it != _characters.end(); ++it ){
				d += (*it)->receiveMagic( npts );
			}

			for ( Items::const_iterator it2 = _items.begin(); it2 != _items.end(); ++it2 ){		
				d += (*it2)->receiveMagic( npts );	
			}

			return d;
		}

		void addTrap(const std::string & nName, unsigned int nPts )
		{
			AbstractItem * itemAbstracto = new Trap(new Item(nName,nPts ));
			_items.push_back(itemAbstracto);

		}

		void once(const std::string & remItem){
            for(Items::iterator it = _items.begin(); it!= _items.end(); it++){
                if((*it)->getName() == remItem){
                    it = _items.erase(it);
                    return;
                }
            }
            throw ItemNotFound();

        }

        void addPotion ( const std::string & newName, unsigned int p )
		{
			
			AbstractItem * absItem = new Potion( new Item(newName, p) ) ;
			_items.push_back( absItem );
		}

		void addBomb ( const std::string & newName, unsigned int p )
		{
			AbstractItem * absItem = new Bomb(new Item(newName, p) );

			_items.push_back( absItem );
		}

		~Location(){
			for(Items::iterator it = _items.begin(); it!= _items.end(); it++){
				delete *it;
			}
		}	

		

};

std::string Item::useItem ( Character * c,  Location * location ) { return c->name() + " uses " + (*this).getName() + " at " + location->name() + "\n"; }

std::string Bomb::useItem( Character * c, Location * l )
{
	if( this->status() != true ){return "Bomb not enabled\n";}

	std::string uses = ItemDecorator::useItem( c, l );
	uses += "Bomb explodes\n";
	uses += l->distributeMagic( 5 );
	l->once(getName());

	return uses;
}


std::string Bomb::receiveMagic( const unsigned int p )
{
	this->status( true );
	std::string receives = ItemDecorator::receiveMagic( p );	
	return receives;
}


std::string Potion::useItem( Character * c ,  Location * l )
{
	std::string uses = ItemDecorator::useItem( c, l );
	l->once(getName());
	return uses +  c->name() + c->cure( 5 );
}


std::string Trap::useItem( Character * c, Location * location )
{
	std::string uses = ItemDecorator::useItem( c, location );
	location->once(getName());
	return uses + c->damage(5);
}

void Character::locateAt (Location & loc)
{
	if (loc_ == NULL) { loc.placeCharacter(*this);  }
	else 
	{
		loc_->unplaceCharacter(*this); 
		loc.placeCharacter(*this); 
	}
	loc_ = &loc;
}


  
class World{ 
	
	Locations _locations;
	Characters _characters;
	
	public:

		~World(){
			for(Locations::iterator it = _locations.begin(); it!= _locations.end(); it++){ delete *it; }
			for(Characters::iterator it = _characters.begin(); it!= _characters.end(); it++){ delete *it; }
		}

		void addLocation (const std::string & l)
		{
			Location* location = new Location();
			location->name(l);
			_locations.push_back(location);
		}
	
		std::string locations () const
		{
			std::string locations = "";
			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){ locations += (*it)->name() + "\n";}
			return locations;
		}
		
		std::string locationDetails( const std::string & details){
			
			std::string locations;
			
			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){
				
				if ((*it)->name() == details){
					locations += (*it)->description();
					return locations; 
				}
			}
			throw LocationNotFound();
		}

		void addItemAtLocation(const std::string &l, const std::string &newItem2, unsigned requiredLevel){
			bool aux= false;
			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){
				
				if ((*it)->name() == l){
					(*it)->addItem(newItem2, requiredLevel);
					aux = true;
				}
			}
			if(aux==false) {throw LocationNotFound();}
		
		}

		void connectNorthToSouth(const std::string &north, const std::string &south){

			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){
				if ((*it)->name() == north){(*it)->connectSouth(findLocation(south));}
				if ((*it)->name() == south){(*it)->connectNorth(findLocation(north));}
			}	
			
		}
		
		void connectWestToEast(const std::string &west, const std::string &east){

			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){
				if ((*it)->name() == west){(*it)->connectEast(findLocation(east));}
				if ((*it)->name() == east){(*it)->connectWest(findLocation(west));}	
			}	
			
		}
		
		
		Location & findLocation ( const std::string & l )
		{
			if ( _locations.empty() != true )
			{	
				for ( Locations::iterator it = _locations.begin(); it != _locations.end();++it )
					if( (*it)->name() == l )
						return (**it);
			}

			throw LocationNotFound();
		}

		void addCharacter (const std::string & c, unsigned level)
		{
			Character* character = new Character();
			character->name(c);
			character->level(level);
			_characters.push_back(character);
		}
		
		std::string characters()const
		{
			std::string characters = "";
	
			for (Characters::const_iterator it = _characters.begin();it != _characters.end();it++ ){
				characters += (*it)->name() + "\n";
			}
	
			return characters; 
		}

		void placeCharacter (const std::string &character,const std::string &location)
		{
			int flagCheckCharacter = 0;
			for (Characters::const_iterator it = _characters.begin();it != _characters.end();it++ ){
				if((*it)->name() == character){
					findCharacter(character).locateAt(findLocation(location));
					flagCheckCharacter=1;
				} 
			}
			if(flagCheckCharacter==0){throw CharacterNotFound();}
		} 
     
		Character & findCharacter ( const std::string & character )
		{ 
			if ( _characters.empty() != true )
			{	
				for ( Characters::iterator it = _characters.begin(); it != _characters.end(); ++it )
					if( (*it)->name() == character )
						return (**it);
			}
  
			throw CharacterNotFound();
		}

		std::string useItem(const std::string locN, const std::string charN, const std::string itN)
		{	
			findLocation(locN);
			findCharacter(charN);
			findLocation(locN).findItem(itN);
			Location & location = findLocation(locN);
			AbstractItem & item = findLocation(locN).findItem(itN); 
			Character & character = findCharacter(charN);
			
			if(character.level() >= item.level()) { return item.useItem(&character, &location); }
			else return "The level of " + character.name() + " is too low\n";
		}
   
		std::string distributeMagic ( std::string nLoc, unsigned int nPts ) { return findLocation( nLoc ).distributeMagic( nPts ); }

		void addDamageCharacter(const std::string & nName, unsigned int nPts)
		{
			Character * character = new CharacterDamage(nName, nPts);
			_characters.push_back(character);
		}

		void addCureCharacter ( const std::string & newName, unsigned int p )
		{
			Character * cureCharacter = new Cure( newName, p );
			_characters.push_back( cureCharacter );
		}

		void addTrapAtLocation (const std::string & nLoc, const std::string & nName, unsigned int nPts ) { return findLocation( nLoc ).addTrap( nName, nPts ); } 
		void addPotionAtLocation ( const std::string & l, const std::string & newName, unsigned int p ) { return findLocation( l ).addPotion( newName, p );}
		void addBombAtLocation ( const std::string & l, const std::string & newName, unsigned int p ) { return findLocation( l ).addBomb( newName, p ); }



};

int main() {

    World world;
    int num;
    int nAux;
    std::string aux1;
    std::string aux2;
    std::string aux3;
    std::string aux4;
    std::string mostrar;
    
    std::cin>>num>>std::ws;
    for(int i=0; i<num; i++){
        std::getline(std::cin,aux1);
        world.addLocation(aux1);
    }
    
    std::cin>>num>>std::ws;
    for(int i=0 ;i<num; i++){
        std::getline(std::cin,aux1,'-');
        std::getline(std::cin,aux2,'-');
        std::cin>>nAux;
        std::cin.ignore();
        std::getline(std::cin,aux3);
        
        if(aux1=="Damage"){ world.addDamageCharacter(aux2,nAux); }
        if(aux1=="Character"){ world.addCharacter(aux2,nAux ); }
        if(aux1=="Cure"){ world.addCureCharacter(aux2,nAux); }
        world.placeCharacter(aux2,aux3 );
    }
    
    std::cin>>num>>std::ws;
    for(int i=0 ;i<num; i++){
        std::getline(std::cin,aux1,'-');
        std::getline(std::cin,aux2,'-');
        std::cin>>nAux;
        std::cin.ignore();
        std::getline(std::cin,aux3);
        
        if(aux1=="Bomb"){ world.addBombAtLocation(aux3,aux2,nAux);}
        if(aux1=="Potion"){ world.addPotionAtLocation(aux3,aux2,nAux); }
        if(aux1=="Trap"){ world.addTrapAtLocation(aux3,aux2,nAux); }
        if(aux1=="Item"){ world.addItemAtLocation(aux3,aux2,nAux); }
    }
    
    std::cin>>num>>std::ws;
    for(int i=0; i<num; i++){
        std::getline(std::cin,aux1,'-');
        if(aux1=="Use"){
            std::getline(std::cin,aux2,'-');
            std::getline(std::cin,aux3,'-');
            std::getline(std::cin,aux4);
            mostrar += world.useItem(aux4,aux2,aux3);   
        }
        if (aux1=="Distribute"){
            std::getline(std::cin,aux4,'-');
            std::cin>>nAux;
            std::cin.ignore();
            mostrar += world.distributeMagic(aux4,nAux);
        }
    }
    std::cout<<mostrar;
}



