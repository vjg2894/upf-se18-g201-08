#ifndef ITEM_HXX
#define ITEM_HXX 

#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <iostream>
 
class Item{

	std::string _name;
	unsigned int _level;
	bool _hasItem;

	public:
		Item() 
			:_name("unknown"), _level(75u), _hasItem(false)	
		{}	

		const std::string & getName() const
		{
			return _name;
		}
                
		void setName(const std::string &name) 
		{
			_name = name;
		}

		const unsigned int & level() const
		{
			return _level;
		}

		void level(const unsigned int &level) 
		{
			_level = level;
		}
		
		const bool & hasItem() const
		{
			return _hasItem;
		}

		void hasItem(const bool &hasItem) 
		{
			_hasItem = hasItem;
		}

};
 

#endif

typedef std::list<Item*> Items;

int main(void) {
    Items objetos;
    int num,i;
    std::string nombreItem,auxiliar;
    unsigned int nivel;
    
    std::cin>>num>>std::ws;   
    for(i=0;i<num;i++){
        std::getline(std::cin,nombreItem,'-');
        std::cin>>nivel;
        std::cin.ignore(); // para no coger el espacio \n del getline
        auxiliar = nombreItem;
        Item *nombreItem = new Item();
        nombreItem->setName(auxiliar);
        nombreItem->level(nivel);
        objetos.push_back(nombreItem);
    }
    for(Items::iterator it=objetos.begin(); it!=objetos.end(); it++){
        std::cout<<"Item: "<<(*it)->getName()<<" ["<<(*it)->level()<<"]"<<"\n";     
    }
}
