#include "Location.hxx"

void Character::locVecina( const std::string & direction )
{
	if ( direction == "East" ) { locateAt( * loc_->east() ); }
	if ( direction == "West" ) { locateAt( * loc_->west() ); }
	if ( direction == "North" ) { locateAt( * loc_->north() ); }
	if ( direction == "South" ) { locateAt( * loc_->south() ); }

}


void Character::locateAt (Location & loc)
{
	if (loc_ == NULL) 
	{ 
		loc.placeCharacter(*this); 
	}
	else 
	{
		loc_->unplaceCharacter(*this); 
		loc.placeCharacter(*this); 
	}
	
	loc_ = &loc;
}



