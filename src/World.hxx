#ifndef WORLD_HXX
#define WORLD_HXX


#include "Item.hxx"
#include "Character.hxx"
#include "Location.hxx"
#include "Cure.hxx" 
#include "Potion.hxx" 
#include "MyException.hxx"
#include "Model.hxx"
#include <vector>

typedef std::vector<Character *> Characters; 
typedef std::vector<Location *> Locations; 


class World : public Model{ 
	
	Locations _locations;
	Characters _characters;
	
	public:

		~World(){
			for(Locations::iterator it = _locations.begin(); it!= _locations.end(); it++){
				delete *it;
			}
			for(Characters::iterator it = _characters.begin(); it!= _characters.end(); it++){
				delete *it;
			}
		}

		void addLocation (const std::string & newLocation)
		{
			Location* location = new Location();
			location->name(newLocation);
			_locations.push_back(location);
		}
	
		std::string locations () const
		{
			std::string locations = "";
	
			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){
				locations += (*it)->name() + "\n";
			}
	
			return locations;
		}
		
		std::string locationDetails( const std::string & details){
			
			std::string locations;
			
			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){
				
				if ((*it)->name() == details){
					locations += (*it)->description();
					return locations; 
				}
			}
			throw LocationNotFound();
		}

		void addItemAtLocation(const std::string &newLocation, const std::string &newItem2, unsigned requiredLevel){
			bool aux= false;
			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){
				
				if ((*it)->name() == newLocation){
					(*it)->addItem(newItem2, requiredLevel);
					aux = true;
				}
			}
			if(aux==false) {throw LocationNotFound();}
		
		}

		void connectNorthToSouth(const std::string &north, const std::string &south){

			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){
				if ((*it)->name() == north){(*it)->connectSouth(findLocation(south));}
				if ((*it)->name() == south){(*it)->connectNorth(findLocation(north));}
			}	
			
		}
		
		void connectWestToEast(const std::string &west, const std::string &east){

			for ( Locations::const_iterator it = _locations.begin(); it != _locations.end(); it++ ){
				if ((*it)->name() == west){(*it)->connectEast(findLocation(east));}
				if ((*it)->name() == east){(*it)->connectWest(findLocation(west));}	
			}	
			
		}
		
		
		Location & findLocation ( const std::string & newLocation )
		{
			if ( _locations.empty() != true )
			{	
				for ( Locations::iterator it = _locations.begin(); it != _locations.end();++it )
					if( (*it)->name() == newLocation )
						return (**it);
			}

			throw LocationNotFound();
		}

		void addCharacter (const std::string & newCharacter, unsigned level)
		{
			Character* character = new Character();
			character->name(newCharacter);
			character->level(level);
			_characters.push_back(character);
		}
		
		std::string characters()const
		{
			std::string characters = "";
	
			for (Characters::const_iterator it = _characters.begin();it != _characters.end();it++ ){
				characters += (*it)->name() + "\n";
			}
	
			return characters; 
		}

		void placeCharacter (const std::string &character,const std::string location)
		{
			int flagCheckCharacter = 0;
			for (Characters::const_iterator it = _characters.begin();it != _characters.end();it++ ){
				if((*it)->name() == character){
					findCharacter(character).locateAt(findLocation(location));
					flagCheckCharacter=1;
				} 
			}
			if(flagCheckCharacter==0){throw CharacterNotFound();}
		} 
     

		Character & findCharacter ( const std::string & character )
		{ 
			if ( _characters.empty() != true )
			{	
				for ( Characters::iterator it = _characters.begin(); it != _characters.end(); ++it )
					if( (*it)->name() == character )
						return (**it);
			}
  
			throw CharacterNotFound();
		}


		std::string useItem(const std::string locN, const std::string charN, const std::string itN)
		{	
			findLocation(locN);
			findCharacter(charN);
			findLocation(locN).findItem(itN);
			Location & location = findLocation(locN);
			AbstractItem & item = findLocation(locN).findItem(itN); 
			Character & character = findCharacter(charN);
			
			if(character.level() >= item.level()) { return item.useItem(&character, &location); }
			else return "The level of " + character.name() + " is too low\n";
		}
   
		std::string distributeMagic ( std::string nLoc, unsigned int nPts )
		{		
			return findLocation( nLoc ).distributeMagic( nPts );
		}

		void addDamageCharacter(const std::string & nName, unsigned nPts)
		{
			Character * character = new CharacterDamage(nName, nPts);
			
			_characters.push_back(character);
		}

		void addTrapAtLocation (const std::string & nLoc, const std::string & nName, unsigned nPts )
		{		
			return findLocation( nLoc ).addTrap( nName, nPts );
		} 

		void addCureCharacter ( const std::string & newName, unsigned int newPoints )
		{
			Character * cureCharacter = new Cure( newName, newPoints );
			_characters.push_back( cureCharacter );
		}

		void addPotionAtLocation ( const std::string & newLocation, const std::string & newName, unsigned newPoints )
		{  
			return findLocation( newLocation ).addPotion( newName, newPoints );
		}

		void addBombAtLocation ( const std::string & newLocation, const std::string & newName, unsigned newPoints )
		{ 
			return findLocation( newLocation ).addBomb( newName, newPoints );
		}


		Character * player;

		void registerPlayer( const std::string & playerName )
		{
			player = & findCharacter(playerName);
		}
	 	
	 	std::string locationDetails() const 
	 	{
	 		return player->playerLocations()->description();
	 	}

		void move( const std::string & direction ) 
		{
			player->locVecina( direction );
		}

	 	std::string useItem( const std::string & itemName ) 
	 	{
	 		return useItem( player->playerLocations()->name(), player->name(), itemName );
	 	}

};

#endif 

